<?php

namespace Baytek\LaravelSortable;

use Illuminate\Support\ServiceProvider;

class SortableServiceProvider extends ServiceProvider {

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/sortable.php' => config_path('sortable.php'),
        ]);
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Sort', function ($app) {
            return new Sort($app);
        });
    }

}