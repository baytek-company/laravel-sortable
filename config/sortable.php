<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Preserve Query String Array
    |--------------------------------------------------------------------------
    |
    | This setting is for a list of query strings to preserve while outputting
    | pagination
    |
    */

    'preserve_query' => [
    	'search',
    	'sort',
    	'order'
    ],
];
